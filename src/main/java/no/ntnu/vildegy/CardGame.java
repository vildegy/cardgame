package no.ntnu.vildegy;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.List;

public class CardGame extends Application {

    Stage window;
    Button dealHandButton;
    Button checkHandButton;
    Scene scene;

    ArrayList<PlayingCard> hand;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        DeckOfCards deckOfCards = new DeckOfCards();

        window = primaryStage;
        window.setTitle("Cardgame");

        Label handDisplay = new Label("");

        checkHandButton = new Button("Check hand");
        dealHandButton = new Button("Deal hand");
        TextField inputText = new TextField();

        Label handFlush = new Label("");
        Label handSum = new Label("");
        Label handCardsOfHearts = new Label("");
        Label handQueenOfSpades = new Label("");

        //Layout
        VBox layout = new VBox(10);
        layout.setPadding(new Insets(20, 20, 20, 20));
        layout.getChildren().addAll(inputText, dealHandButton, checkHandButton, handDisplay, handFlush,handQueenOfSpades,handSum,handCardsOfHearts);

        /**
         * Method when pushing the "dealHandButton"
         */
        dealHandButton.setOnAction(e -> {
            int numberOfCardsOnHand = Integer.parseInt(inputText.getText());
            if (deckOfCards.getDeck().size()<numberOfCardsOnHand) {
                handDisplay.setText("Not enough cards left in deck to deal hand");
            } else {
                this.hand = deckOfCards.dealHand(numberOfCardsOnHand);
            String handString = "";
            for (PlayingCard p : hand) {
                handString += p.getAsString() + "; ";
            }
            handDisplay.setText("The cards on hand are: " + handString);
        }
    });

        /**
         * Method when pushingf the check hand button
         */
        checkHandButton.setOnAction(e -> {
            CheckHand checkHand = new CheckHand(hand);
            boolean flush = checkHand.checkFlush();
            if (flush){
                handFlush.setText("Flush: Yes");
            }
            else {
                handFlush.setText("Flush: No");
            }

            handSum.setText("Sum of faces: " + checkHand.sumOfFaces());
            List<PlayingCard> cardsOfHearts = checkHand.cardsOfHearts();
            String cardOfHeartsString = "";
            for (PlayingCard p : cardsOfHearts){
                cardOfHeartsString += p.getAsString() + "; ";
            }
            handCardsOfHearts.setText("Cards of hearts: " + cardOfHeartsString);

            if (checkHand.queenOfSpades()){
                handQueenOfSpades.setText("Queen of spades: Yes");
            }
            else {
                handQueenOfSpades.setText("Queen of spades: No");
            }

        });

        scene = new Scene(layout, 400, 350);
        window.setScene(scene);
        window.show();
    }
}
