package no.ntnu.vildegy;
/**
 * This class contains methods that analysis the cards on hand
 * When the user push the button "check hand" on the application
 */

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CheckHand {

    private ArrayList<PlayingCard> hand;


    public CheckHand(ArrayList<PlayingCard> hand){
        this.hand = hand;
    }

    /**
     * Checks the hand for flush
     * a flush is a hand of playing cards where all cards are of the same suit
     * @return true/false depending on whether it is flush or not
     */
    public boolean checkFlush(){
        char suit = hand.get(0).getSuit();
        List<PlayingCard> flushCheck = hand.stream().filter(p -> p.getSuit()==suit).collect(Collectors.toList());
        return flushCheck.size() == hand.size();
    }

    /**
     * Calculate the sum of faces on the hand
     * Ace = 1
     * @return the sum of faces as an int
     */
    public int sumOfFaces(){
        int sum = hand.stream().map(PlayingCard::getFace).reduce((a,b) -> a+b).get();
        return sum;
    }

    /**
     * Picks out all the cards on hand that are of the suit hearts
     * @return a List with all the cards that are of suit hearts
     */
    public List<PlayingCard> cardsOfHearts() {
        return hand.stream().filter(p -> p.getSuit() == 'H').collect(Collectors.toList());
        }


    /**
     * Checks if the card "S12"/ "spar dame" is on hand
     * @return true if it is, false if not
     */
    public boolean queenOfSpades(){
        return hand.stream().anyMatch(p -> p.getAsString().equals("S12"));
    }
}
