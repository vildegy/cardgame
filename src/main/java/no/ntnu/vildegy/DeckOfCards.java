package no.ntnu.vildegy;

/**
 * Represent a deck of cards. A deck of cards has 52 playing cards.
 * The class has a method that draws n numbers of random cards from the deck
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class DeckOfCards {

    private final char[] SUIT= {'S','D','C','H'};
    private final int[] FACE = {1,2,3,4,5,6,7,8,9,10,11,12,13};
    private ArrayList<PlayingCard> deck = new ArrayList<>();

    /**
     * Constructor, creates every 52 cards in the deck
     */
    public DeckOfCards() {
        for(int i = 0; i < SUIT.length; i++) {
            for (int j = 0; j < FACE.length; j++) {
                PlayingCard playingCard = new PlayingCard(SUIT[i], FACE[j]);
                deck.add(playingCard);
            }
        }
    }

    public char[] getSuit() {
        return SUIT;
    }

    public int[] getFace() {
        return FACE;
    }

    public ArrayList<PlayingCard> getDeck() {
        return deck;
    }

    /** Method that draws n numbers of random cards from the deck
     *
     * @param n number of random cards to be drawn (integer between 1 and 52)
     * @return a list with n number of random cards
     */
    public ArrayList<PlayingCard> dealHand(int n) {
        if (n < 1 || n > 52) {
            throw new IllegalArgumentException("Number must be between 1 and 52");
        } else {
            Random random = new Random();
            ArrayList<PlayingCard> randomCards = new ArrayList();

            for (int i = 0; i < n; i++) {
                randomCards.add(deck.get(random.nextInt(deck.size()))); //finner n ant random elements fra deck og adder i randomCards
            }
            return randomCards;
        }
    }

    /**
     *
     * @return the suit and face to the card
     */
    @Override
    public String toString() {
        return "DeckOfCards " +
                Arrays.toString(SUIT) + Arrays.toString(FACE);
    }

}
